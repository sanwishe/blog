# 使用expect处理shell脚本中的交互


## 背景

linux shell脚本中经常会遇到需要用户输入密码，或者选择yes/no的情况，这事脚本会中断执行等待用户输入，然而这与自动化的理念相悖。我们需要通过某种方式代替这些用户手动输入，即自动填充用户输入部分，是的脚本继续执行下去。本文源于在paas平台上通过ansible自动化部署mysql时，ubuntu下的mysql安装需要用户输入密码等操作，中断了自动化脚本执行。基于此问题，本文演示了一种通过**expect**脚本自动填充mysql安装时用户输入的方法。

## expect简介

Expect是Unix系统中用来进行自动化控制和测试的软件工具，作为Tcl脚本语言的一个扩展，应用在交互式软件中如telnet，ftp，Passwd，fsck，rlogin，tip，ssh等，Expect可以根据程序的提示 模拟标准输入给程序提供信息来实现交互程序执行。Expect需要Tcl编程语言的支持，要在系统上运行Expect必须首先安装Tcl。

## expect简单使用

Expect是在Tcl基础上创建起来的，它还提供了一些Tcl所没有的命令，下面是本文使用到的expect脚本：

##### 示例1

```bash
#!/usr/bin/expect
set timeout 30
set pkg_name [lindex $argv 0]
set root_passwd [lindex $argv 1]
spawn dpkg -i $pkg_name
expect "<ok>"
send "\n\r"
expect "Enter root password"
send "$root_passwd\r"
expect "Re-enter root password"
send "$root_passwd\r"
expect eof
```

###### `#!/usr/bin/expect`

这一行告诉操作系统脚本里的代码使用那一个shell来执行。这里的expect其实和linux下的bash、windows下的cmd是一类东西。 注意：这一行需要在脚本的第一行。

###### `set timeout 30`

设置超时时间，计时单位是秒。timeout -1 为永不超时。

###### `set pkg_name [lindex $argv 0]`

[lindex $argv 0]表示脚本执行的第一个参数，从0开始计数。

###### `spawn dpkg -i $pkg_name`

spawn是进入expect环境后才可以执行的expect内部命令，由shell自带。它主要的功能是给ssh运行进程加个壳，用来传递交互指令。

###### `expect  < ok >`

这里的expect也是expect的一个内部命令，expect的shell命令和内部命令是一样的，但不是一个功能，这个命令的意思是判断上次输出结果里是否包含“< ok >”的字符串，如果有则立即返回，否则就等待一段时间后返回，这里等待时长就是前面设置的30秒。

###### `send "\n\r"`

这里就是执行交互动作，与手工输入密码的动作等效。 
注意命令字符串结尾别忘记加上“\r”。

###### `expect eof`

如果你只是希望执行完上述指令之后推出，只需要加上expect eof即可；在ssh、telnet等交互操作中如果希望执行完成上述命令后保持在交互状态，需要将expect eof替换为interact。此时Expect会把控制权交给控制台，这个时候就变回手工操作，Expect已经执行完成。

## 使用expect自动处理mysql安装过程中的交互

#### 准备

- 安装expect

ubuntu14.04环境下安装expect命令：

```bash
root@master:/home/mysql# apt-get install expect
```

如果是没有网络的环境，可以下载deb包离线安装，安装expect需要依赖tcl，从网上下载一下安装包：libtcl8.6_8.6.1-4ubuntu1_amd64.deb；expect_5.45-5ubuntu1_amd64.deb。

通过ftp上传到节点上，通过`dpkg -i $packagename`安装。

- 安装ansible

ubuntu下通过apt-get install ansible安装的ansible版本过低，只有1.5.4，这里推荐使用python pip安装，
首先安装pip，命令：`apt-get install python-pip`

使用pip安装ansible，命令：`pip install ansible`

- mysql自动化安装脚本

mysql离线安装需要4个deb安装包和两个依赖包，其中在安装mysql-community-server包时，会提示用户输入回车和密码操作，这部分交互的自动化处理流程见示例1。
将示例1保存为install_server.exp（MYSQL_EXPECT=install_server.exp），并在mysql安装脚本中嵌入该脚本，具体代码如下：

##### 示例2

```bash
#!/bin/bash
    
EXPECT_DEB_1=libtcl8.6_8.6.1-4ubuntu1_amd64.deb
EXPECT_DEB_2=expect_5.45-5ubuntu1_amd64.deb
MYSQL_DEPENDENCIES_1=libaio1_0.3.110-2_amd64.deb
MYSQL_DEPENDENCIES_2=libmecab2_0.996-1.1_amd64.deb
MYSQL_COMMON_DEB=mysql-common_5.7.12-1ubuntu14.04_amd64.deb
MYSQL_CLIENT_DEB=mysql-client_5.7.12-1ubuntu14.04_amd64.deb
MYSQL_CLIENT_CE_DEB=mysql-community-client_5.7.12-1ubuntu14.04_amd64.deb
MYSQL_SERVER_CE_DEB=mysql-community-server_5.7.12-1ubuntu14.04_amd64.deb
MYSQL_PASSWORD=root
MYSQL_EXPECT=install_server.exp
    
function hasDpkg
{
    r=`dpkg -l | grep "$1"`
    if [ -n "$r" ]
        then
            h=`dpkg -l | grep "ii  $1"`
            if [ -n "$h" ]
            then
                return 1
            else
                return 0
            fi
        else
            return 0
        fi
}
    
mysql="mysql-server-5.7"
    
    
#BIGIN TO INSTALL MYSQL
#step1 CHECK IF MYSQL INSTALLED AND REMOVE IT  
hasDpkg $mysql

r=$?

if [ $r -eq 1 ]
    then
        echo "$mysql was installed"  
    else
        echo "$mysql was not installed"  
    
        #step2 INSTALL EXPECT
        echo "step2 INSTALL EXPECT"
        dpkg -i $EXPECT_DEB_1
        dpkg -i $EXPECT_DEB_2
        echo "expect has benn installed successfully!"
    
        #step3 INSTALL DEPENDENCIES
        echo "step3 INSTALL DEPENDENCIES"
        dpkg -i $MYSQL_DEPENDENCIES_1
        dpkg -i $MYSQL_DEPENDENCIES_2
        echo "mysql dependencies packages were installed successfully"
    
        #INSTALL MYSQL PACKAGE
        echo "step3 INSTALL MYSQL PACKAGE"
        dpkg -i $MYSQL_COMMON_DEB
        dpkg -i $MYSQL_CLIENT_CE_DEB
        dpkg -i $MYSQL_CLIENT_DEB
    
        chmod +x $MYSQL_EXPECT
        ./$MYSQL_EXPECT $MYSQL_SERVER_CE_DEB $MYSQL_PASSWORD
    
        echo "mysql has been newly installed "
    fi
    
#step4 SETUP MYSQL
echo "step4 SETUP MYSQL"
service mysql start
    
echo "Mysql installation finished!!"
```

示例2中的65行嵌入了expect脚本，传入安装包名称和root密码。

- 定制ansible脚本

- 部署：

执行`ansible-playbook -i hosts deploy.yml`


```bash
root@master:~/mysql# ansible-playbook -i hosts deploy.yml 
    
PLAY [node1] *******************************************************************
    
TASK [setup] *******************************************************************
ok: [10.63.212.83]
ok: [10.63.212.84]

TASK [include] *****************************************************************
included: /root/mysql/task.yml for 10.63.212.84, 10.63.212.83

TASK [create mysql install tmp path] *******************************************
ok: [10.63.212.83]
ok: [10.63.212.84]
    
TASK [create mysql initial tmp path] *******************************************
ok: [10.63.212.83]
ok: [10.63.212.84]
    
TASK [unarchive mysql_inst.tar.gz] *********************************************
changed: [10.63.212.84]
changed: [10.63.212.83]
      
TASK [copy install script] *****************************************************
changed: [10.63.212.83]
changed: [10.63.212.84]
    
TASK [copy initial script] *****************************************************
changed: [10.63.212.83]
changed: [10.63.212.84]
    
TASK [unarchive mysql_init.tar.gz] *********************************************
changed: [10.63.212.83]
changed: [10.63.212.84]
    
TASK [install mysql] ***********************************************************
changed: [10.63.212.83]
changed: [10.63.212.84]
    
TASK [initial mysql] ***********************************************************
changed: [10.63.212.84]
changed: [10.63.212.83]
    
PLAY RECAP *********************************************************************
10.63.212.83               : ok=10   changed=6    unreachable=0    failed=0   
10.63.212.84               : ok=10   changed=6    unreachable=0    failed=0   
```
    
- 验证

登陆到节点，执行mysql相关操作：


```bash
root@node1:~# mysql -u root -proot
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 5
Server version: 5.7.12 MySQL Community Server (GPL)
    
Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.
    
Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.
    
Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| cn_pm              |
| mysql              |
| performance_schema |
| sys                |
| zenap_fm           |
| zenap_license      |
| zenap_res          |
| zenap_sm           |
| zenap_topo         |
+--------------------+
10 rows in set (0.00 sec)
    
mysql> exit
Bye	
```


至此，mysql自动化部署完成。

## 总结

本文在之前mysql部署的基础上，完成了mysql在ubuntu下的部署。其中涉及到交互的部分采用expect处理之。本文为shell中自动化处理交互提供了参考。